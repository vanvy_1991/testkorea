package com.test_korena.custom_view.bottommenu

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.test_korena.R
import kotlinx.android.synthetic.main.layout_bottom_menu_item.view.*

class BottomMenuItem @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private var normalImage: Drawable? = null
    private var selectedImage: Drawable? = null

    private var normalTitleColor: Int = 0
    private var selectedTitleColor: Int = 0

    val title: String
        get() = tvTitle.text.toString()

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.layout_bottom_menu_item, this, true)

        val ta = context.obtainStyledAttributes(attrs, R.styleable.BottomMenuItem)
        val text = ta.getString(R.styleable.BottomMenuItem_title)

        normalImage = ta.getDrawable(R.styleable.BottomMenuItem_normalImage)
        selectedImage = ta.getDrawable(R.styleable.BottomMenuItem_selectedImage)

        normalTitleColor = ta.getColor(
            R.styleable.BottomMenuItem_normalTitleColor,
            Color.GRAY
        ) // default color is gray
        selectedTitleColor = ta.getColor(
            R.styleable.BottomMenuItem_selectedTitleColor,
            Color.BLUE
        ) //  default selected color is blue

        ta.recycle()
        if (normalImage != null) {
            imgImage.setImageDrawable(normalImage)
        }
        tvTitle.text = text
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        imgImage.drawable.setTint(
            ContextCompat.getColor(context, if (selected) R.color.tea_lish else R.color.afb5b5)
        )
        tvTitle.setTextColor(
            ContextCompat.getColor(
                context,
                if (selected) R.color.tea_lish else R.color.afb5b5
            )
        )
    }
}