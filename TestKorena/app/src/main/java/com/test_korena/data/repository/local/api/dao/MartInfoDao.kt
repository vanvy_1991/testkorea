package com.test_korena.data.repository.local.api.dao

import androidx.room.*
import com.test_korena.data.model.DetailPage
import com.test_korena.data.model.local.CategoryLocal
import com.test_korena.data.model.local.MartInfoLocal

@Dao
abstract class MartInfoDao {

    @androidx.room.Query("DELETE FROM CategoryLocal")
    abstract fun deleteCategories(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveCategories(categoryLocal: CategoryLocal): Long

    @Transaction
    open fun updateCategories(categoryLocal: CategoryLocal) {
        deleteCategories()
        saveCategories(categoryLocal)
    }

    @Query("SELECT * FROM CategoryLocal")
    abstract fun getCategoriesLocal(): CategoryLocal

    @androidx.room.Query("DELETE FROM MartInfoLocal WHERE userId LIKE :userId AND categoryId LIKE :categoryId AND pageNum LIKE :pageNum ")
    abstract fun deleteMartInfo(categoryId: String, pageNum: Int, userId: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveMartInfo(martInfoLocal: MartInfoLocal): Long

    @Transaction
    open fun updateMartInfo(martInfoLocal: MartInfoLocal) {
        deleteMartInfo(martInfoLocal.categoryId, martInfoLocal.pageNum, martInfoLocal.userId)
        saveMartInfo(martInfoLocal)
    }

    @Query("SELECT * FROM MartInfoLocal WHERE userId LIKE :userId AND categoryId LIKE :categoryId AND pageNum LIKE :pageNum")
    abstract fun getMartInfoLocal(categoryId: String, pageNum: Int, userId: Int): MartInfoLocal

    @androidx.room.Query("DELETE FROM DetailPage WHERE userId LIKE :userId AND boardId LIKE :boardId ")
    abstract fun deleteDetailPage(boardId: Int, userId: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveDetailPage(detailPage: DetailPage): Long

    @Transaction
    open fun updateDetailPage(detailPage: DetailPage) {
        deleteDetailPage(detailPage.boardId, detailPage.userId)
        saveDetailPage(detailPage)
    }

    @Query("SELECT * FROM DetailPage WHERE userId LIKE :userId AND boardId LIKE :boardId")
    abstract fun getDetailPageLocal(boardId: Int, userId: Int): DetailPage
}