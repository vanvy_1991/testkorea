package com.test_korena.custom_view.bottommenu

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.cardview.widget.CardView
import com.test_korena.R
import kotlinx.android.synthetic.main.layout_bottom_menu.view.*

class BottomMenu @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {
    var onMenuItemSelectedListener: OnMenuItemSelectedListener? = null
    private var onMenuChangedListener: OnMenuChanged? = null
    private var menuItemArray: Array<BottomMenuItem?> = arrayOf()

    init {
        init()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.layout_bottom_menu, this, true)
        initMenu()
        handleMenuItemClick()
    }

    private fun initMenu() {
        menuItemArray = arrayOfNulls(layoutGroupMenu.childCount)
        for (i in 0 until layoutGroupMenu.childCount) {
            menuItemArray[i] = layoutGroupMenu.getChildAt(i) as BottomMenuItem
        }
    }

    private fun handleMenuItemClick() {
        for (i in menuItemArray.indices) {
            menuItemArray[i]?.setOnClickListener {
                onMenuItemSelectedListener?.onMenuItemSelected(it as BottomMenuItem)
            }
        }
    }

    fun select(menuId: Int) {
        reset()
        findMenuById(menuId)?.isSelected = true
        onMenuChangedListener?.onMenuChanged()
    }

    private fun reset() {
        for (i in menuItemArray.indices) {
            getMenuItem(i)?.isSelected = false
        }
    }

    private fun findMenuById(id: Int): BottomMenuItem? {
        for (menu in menuItemArray) {
            if (menu?.id == id) {
                return menu
            }
        }
        return null
    }

    private fun getMenuItem(position: Int): BottomMenuItem? {
        return menuItemArray[position]
    }

    fun size(): Int {
        return menuItemArray.size
    }

    /**
     * Callback invoke when we click a menu item
     */
    interface OnMenuItemSelectedListener {
        fun onMenuItemSelected(menuItem: BottomMenuItem)
    }

    /**
     * Callback invoke when we select a menu item
     */
    interface OnMenuChanged {
        fun onMenuChanged()
    }
}