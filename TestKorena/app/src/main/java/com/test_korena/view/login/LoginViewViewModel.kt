package com.test_korena.view.login

import androidx.lifecycle.MutableLiveData
import com.test_korena.data.model.User
import com.test_korena.data.repository.LoginRepository
import com.test_korena.view.base.BaseViewModel
import javax.inject.Inject

class LoginViewViewModel @Inject constructor(private val loginRepository: LoginRepository) :
    BaseViewModel() {
    var dataLoginSuccess = MutableLiveData<User>()
    var dataLoginError = MutableLiveData<Throwable>()
    var isShowLoading = MutableLiveData<Boolean>()

    fun login(memberId: String, memberPassword: String) {
        compositeDisposable.add(loginRepository.login(memberId, memberPassword)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }.doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { user ->
                    dataLoginSuccess.value = user
                }
            }, {
                dataLoginError.value = it
            })
        )
    }

}