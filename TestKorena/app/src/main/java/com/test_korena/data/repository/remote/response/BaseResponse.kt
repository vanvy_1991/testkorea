package com.test_korena.data.repository.remote.response

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseResponse : Parcelable {
    @SerializedName("code")
    @Expose
    var code: Int? = 0

    @SerializedName("code_msg")
    @Expose
    var message: String? = ""
}