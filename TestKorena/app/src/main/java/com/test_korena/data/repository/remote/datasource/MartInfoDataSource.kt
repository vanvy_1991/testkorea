package com.test_korena.data.repository.remote.datasource

import com.test_korena.data.model.DetailPage
import com.test_korena.data.repository.remote.api.RemoteApi
import com.test_korena.data.repository.remote.response.CategoryResponse
import com.test_korena.data.repository.remote.response.MartInfoResponse
import io.reactivex.Single
import javax.inject.Inject

class MartInfoDataSource @Inject constructor(private val api: RemoteApi) : BaseRemoteDataSource() {

    fun getCategories(): Single<CategoryResponse> {
        return api.getCategories()
    }

    fun getMartInfo(idUser: Int, page: Int, categoryId: String): Single<MartInfoResponse> {
        return api.getMartInfo(idUser, page, categoryId)
    }

    fun getMartDetail(idUser: Int, idItem: Int): Single<DetailPage> {
        return api.getMartDetail(idUser, idItem)
    }
}