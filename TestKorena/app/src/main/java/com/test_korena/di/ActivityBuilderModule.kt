package com.test_korena.di

import com.test_korena.di.scope.ActivityScope
import com.test_korena.view.detail.DetailPageActivity
import com.test_korena.view.home.HomeActivity
import com.test_korena.view.home.HomeViewModule
import com.test_korena.view.login.LoginActivity
import com.test_korena.view.login.LoginViewModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * The [Module] class declares how to inject an Activity instance into corresponding
 * {@link Module}
 */
@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    @ActivityScope
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [HomeViewModule::class])
    @ActivityScope
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector()
    @ActivityScope
    abstract fun bindDetailPageActivity(): DetailPageActivity
}
