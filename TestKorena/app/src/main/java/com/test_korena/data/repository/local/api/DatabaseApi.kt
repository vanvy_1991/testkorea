package com.test_korena.data.repository.local.api

import com.test_korena.data.repository.local.api.dao.MartInfoDao
import com.test_korena.data.repository.local.api.dao.TestDao

interface DatabaseApi {

    fun testDAO(): TestDao
    fun martInfoDAO(): MartInfoDao
}