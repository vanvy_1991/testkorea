package com.test_korena.data.repository.remote.datasource

import com.test_korena.data.model.User
import com.test_korena.data.repository.LoginRepository
import com.test_korena.data.repository.remote.api.RemoteApi
import io.reactivex.Single
import javax.inject.Inject

class LoginRemoteDataSource @Inject constructor(private val remoteApi: RemoteApi) :
    BaseRemoteDataSource() {

    fun login(memberId: String, memberPassword: String): Single<User> {
        return remoteApi.login(memberId, memberPassword, "2", "A")
    }
}