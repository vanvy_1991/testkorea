package com.test_korena.data.model

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 *  {
"board_idx": "17",
"board_type": "board",
"ins_date": "2020.05.05",
"title": "5월 제철식재료 딸기",
"img_path": "http://dev-admin.martjangbogo.com/media/commonfile/202005/08/f891bab61fabf768e98b6d1f95ab7d41.png",
"reply_cnt": "0",
"like_cnt": "0",
"my_like_yn": "N",
"category": "오늘뭐먹지",
"contents_yn": "Y"
}
 */

@Parcelize
data class MartInfo constructor(
    @SerializedName("board_idx")
    @Expose
    var id: Int,
    @SerializedName("board_type")
    @Expose
    var type: String,
    @SerializedName("ins_date")
    @Expose
    var inDate: String,
    @SerializedName("title")
    @Expose
    var title: String,
    @SerializedName("img_path")
    @Expose
    var imgPath: String,
    @SerializedName("reply_cnt")
    @Expose
    var replyComment: String,
    @SerializedName("like_cnt")
    @Expose
    var likeComment: String,
    @SerializedName("my_like_yn")
    @Expose
    var isMyLike: String,
    @SerializedName("category")
    @Expose
    var category: String,
    @SerializedName("contents_yn")
    @Expose
    var isMyContent: String
) : Parcelable {
}