package com.test_korena.view.home

import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.test_korena.R
import com.test_korena.custom_view.bottommenu.BottomMenu
import com.test_korena.custom_view.bottommenu.BottomMenuItem
import com.test_korena.view.base.BaseActivity
import com.test_korena.view.home.martinfo.MartInfoViewFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity() {
    private var lastShownFragmentTag: String = ""

    private var lastShownFragmentPos: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // In Activity's onCreate() for instance
        setContentView(R.layout.activity_home)
        initView()
    }

    private fun initView() {
        bottomMenu.onMenuItemSelectedListener = object : BottomMenu.OnMenuItemSelectedListener {
            override fun onMenuItemSelected(menuItem: BottomMenuItem) {
                hideLastShownFragment()
                lastShownFragmentPos = menuItem.id
                when (menuItem.id) {
                    R.id.menuHomItem -> goToHome()
                    R.id.menuMartInfoItem -> goToMartInfo()
                    R.id.menuChannelItem -> goToChannel()
                    R.id.menuMoreItem -> goToMore()
                    else -> throw IllegalArgumentException("Incorrect menu item id")
                }
            }
        }
        // For default
        lastShownFragmentPos = R.id.menuMartInfoItem

        goToMartInfo()
    }

    private fun hideLastShownFragment() {
        val lastShowFragment: Fragment? =
            supportFragmentManager.findFragmentByTag(lastShownFragmentTag)
        lastShowFragment?.let {
            navigator.hideFragment(supportFragmentManager, lastShowFragment)
        }
    }

    private fun backToCurrentTab() {
        val lastShowFragment: Fragment? =
            supportFragmentManager.findFragmentByTag(lastShownFragmentTag)
        navigator.showLastFragmentInBackStack(lastShowFragment)
    }

    private fun goToHome() {
        showFragment("Tab1")
        bottomMenu.select(R.id.menuHomItem)
    }

    private fun goToMartInfo() {
        showFragment(MartInfoViewFragment::class.java.simpleName)
        bottomMenu.select(R.id.menuMartInfoItem)
    }

    private fun goToChannel() {
        showFragment("Tab3")
        bottomMenu.select(R.id.menuChannelItem)
    }

    private fun goToMore() {
        showFragment("Tab4")
        bottomMenu.select(R.id.menuMoreItem)
    }

    private fun showFragment(tag: String): Boolean {
        val isShowFragment: Boolean
        val targetFragment: Fragment? = supportFragmentManager.findFragmentByTag(tag)
        isShowFragment = if (targetFragment != null) {
            navigator.showFragment(supportFragmentManager, targetFragment)
            true
        } else {
            navigator.addFragment(
                supportFragmentManager, createNewFragmentByTag(tag),
                R.id.contentFrame, tag = tag
            )
            true
        }
        lastShownFragmentTag = tag
        return isShowFragment
    }

    private fun createNewFragmentByTag(tag: String): Fragment {
        return when (tag) {
            MartInfoViewFragment::class.java.simpleName -> MartInfoViewFragment.newInstance()
            else -> Fragment()
        }
    }
}