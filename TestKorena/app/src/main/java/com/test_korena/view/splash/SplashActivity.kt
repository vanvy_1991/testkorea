package com.test_korena.view.splash

import android.os.Bundle
import com.test_korena.R
import com.test_korena.view.base.BaseActivity

class SplashActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }
}