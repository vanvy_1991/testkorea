package com.test_korena.data.model.local

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.test_korena.data.model.MartInfo
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "MartInfoLocal")
@Parcelize
data class MartInfoLocal(
    @TypeConverters(ListTypeMartInfoConverters::class)
    var martInfo: List<MartInfo>,
    var categoryId: String,
    var pageNum: Int,
    var userId: Int
) : Parcelable {

    @PrimaryKey(autoGenerate = true)
    var primaryKey: Int = 0
}