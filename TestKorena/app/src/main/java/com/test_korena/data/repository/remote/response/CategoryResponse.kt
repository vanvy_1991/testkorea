package com.test_korena.data.repository.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.test_korena.data.model.Category
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryResponse constructor(
    @SerializedName("data_array")
    @Expose
    var categories: MutableList<Category>
) : BaseResponse() {
}