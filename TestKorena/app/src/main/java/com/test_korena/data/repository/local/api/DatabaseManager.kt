package com.test_korena.data.repository.local.api

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.test_korena.data.model.DetailPage
import com.test_korena.data.model.TestModel
import com.test_korena.data.model.local.CategoryLocal
import com.test_korena.data.model.local.ListTypeConverters
import com.test_korena.data.model.local.ListTypeMartInfoConverters
import com.test_korena.data.model.local.MartInfoLocal
import com.test_korena.data.repository.local.api.DatabaseManager.Companion.DATABASE_VERSION
import com.test_korena.data.repository.local.api.dao.MartInfoDao
import com.test_korena.data.repository.local.api.dao.TestDao

@Database(
    entities = [TestModel::class, MartInfoLocal::class, DetailPage::class, CategoryLocal::class],
    version = DATABASE_VERSION
)
@TypeConverters(ListTypeConverters::class, ListTypeMartInfoConverters::class)
abstract class DatabaseManager : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "TestKoreaDataBase"
        const val DATABASE_VERSION = 1
    }

    abstract fun testDAO(): TestDao
    abstract fun martInfoDAO(): MartInfoDao
}
