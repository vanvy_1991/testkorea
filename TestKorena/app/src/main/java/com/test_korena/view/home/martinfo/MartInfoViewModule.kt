package com.test_korena.view.home.martinfo

import com.test_korena.data.repository.MartInfoRepository
import com.test_korena.di.scope.FragmentScope
import dagger.Module
import dagger.Provides

@Module
class MartInfoViewModule {
    @Provides
    @FragmentScope
    internal fun provideViewModel(martInfoRepository: MartInfoRepository): MartInfoViewViewModel {
        return MartInfoViewViewModel(martInfoRepository)
    }
}