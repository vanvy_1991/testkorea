package com.test_korena.data.model.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test_korena.data.model.Category
import java.util.*


class ListTypeConverters {
    companion object {
        var gson = Gson()

        @TypeConverter
        @JvmStatic
        fun stringToSomeObjectList(data: String?): List<Category> {
            if (data == null) {
                return Collections.emptyList()
            }

            val listType = object : TypeToken<List<Category>>() {
            }.type

            return gson.fromJson<List<Category>>(data, listType)
        }

        @TypeConverter
        @JvmStatic
        fun someObjectListToString(someObjects: List<Category>): String {
            return gson.toJson(someObjects)
        }
    }

}