package com.test_korena.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * {"code":"1000","code_msg":"\uc815\uc0c1","member_idx":"143","member_id":"ttt@gmail.com","member_name":null}
 */
@Parcelize
data class User constructor(
    @SerializedName("code")
    @Expose
    var code: Int,
    @SerializedName("code_msg")
    @Expose
    var codeMsg: String,
    @SerializedName("member_idx")
    @Expose
    var memberIdx: Int,
    @SerializedName("member_id")
    @Expose
    var memberId: String,
    @SerializedName("member_name")
    @Expose
    var memberName: String
) : Parcelable {
}