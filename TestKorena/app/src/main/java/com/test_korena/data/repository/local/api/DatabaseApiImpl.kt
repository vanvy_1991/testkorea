package com.test_korena.data.repository.local.api

import com.test_korena.data.repository.local.api.dao.MartInfoDao
import com.test_korena.data.repository.local.api.dao.TestDao

class DatabaseApiImpl(private val databaseManager: DatabaseManager) :
    DatabaseApi {

    override fun testDAO(): TestDao {
        return databaseManager.testDAO()
    }

    override fun martInfoDAO(): MartInfoDao {
       return databaseManager.martInfoDAO()
    }
}