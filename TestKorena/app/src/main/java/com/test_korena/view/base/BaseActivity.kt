package com.test_korena.view.base

import android.util.Log
import com.test_korena.data.repository.local.api.SharedPrefApi
import com.test_korena.utils.navigation.Navigator
import com.test_korena.utils.viewextension.gone
import com.test_korena.utils.viewextension.visible
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.layout_header.*
import javax.inject.Inject

open class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var sharedPrefApi: SharedPrefApi

    private var savedInstanceStateDone: Boolean = false

    fun setHeader(
        isShowIconLeft: Boolean,
        title: String,
        isShowIconRight: Boolean
    ) {
        if (isShowIconLeft) {
            ivBack.visible()
        } else {
            ivBack.gone()
        }
        Log.e("xxx", title)
        tvTitleHeader.text = title
        if (isShowIconRight) {
            ivRight.visible()
        } else {
            ivRight.gone()
        }
    }

}

