package com.test_korena.data.model.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test_korena.data.model.Category
import com.test_korena.data.model.MartInfo
import java.util.*


class ListTypeMartInfoConverters {
    companion object {
        var gson = Gson()

        @TypeConverter
        @JvmStatic
        fun stringToSomeObjectList(data: String?): List<MartInfo> {
            if (data == null) {
                return Collections.emptyList()
            }

            val listType = object : TypeToken<List<MartInfo>>() {
            }.type

            return gson.fromJson<List<MartInfo>>(data, listType)
        }

        @TypeConverter
        @JvmStatic
        fun someObjectListToString(someObjects: List<MartInfo>): String {
            return gson.toJson(someObjects)
        }
    }

}