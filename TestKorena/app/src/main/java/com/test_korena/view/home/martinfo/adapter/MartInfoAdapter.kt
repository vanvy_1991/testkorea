package com.test_korena.view.home.martinfo.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test_korena.R
import com.test_korena.data.model.MartInfo
import com.test_korena.utils.imageloader.loadImage
import com.test_korena.utils.viewextension.safeClick
import kotlinx.android.synthetic.main.layout_bottom_mart_info.view.*
import kotlinx.android.synthetic.main.layout_item_mart.view.*


class MartInfoAdapter constructor(
    private var listMartInfo: MutableList<MartInfo>,
    private val listener: OnItemMartInfoClickListener
) : RecyclerView.Adapter<MartInfoAdapter.ItemMartViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemMartViewHolder {
        return ItemMartViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_item_mart, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listMartInfo.size
    }

    override fun onBindViewHolder(holder: ItemMartViewHolder, position: Int) {
        holder.onBind(listMartInfo[position], position, listener)
    }

    class ItemMartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun onBind(martInfo: MartInfo, position: Int, listener: OnItemMartInfoClickListener) {
            with(itemView) {
                tvCategoryName.text = martInfo.category
                tvTitle.text = martInfo.title
                tvInDate.text = martInfo.inDate
                tvComment.text = martInfo.replyComment
                tvLike.text = martInfo.likeComment
                loadImage(ivItem, martInfo.imgPath)
                if (martInfo.isMyLike == "Y") {
                    ivLike.setImageResource(R.drawable.heart_active)
                } else {
                    ivLike.setImageResource(R.drawable.heart)
                }

                if (martInfo.isMyContent == "Y") {
                    ivComment.setImageResource(R.drawable.comment_active)
                } else {
                    ivComment.setImageResource(R.drawable.comment)
                }
            }
            itemView.safeClick(View.OnClickListener {
                listener.onItemMartInfoClick(position)
            })
        }
    }


    interface OnItemMartInfoClickListener {
        fun onItemMartInfoClick(position: Int)
    }
}