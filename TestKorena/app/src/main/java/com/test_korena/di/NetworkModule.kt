package com.test_korena.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.test_korena.BuildConfig
import com.test_korena.data.repository.local.api.AccessTokenWrapper
import com.test_korena.data.repository.remote.api.RemoteApi
import com.test_korena.data.repository.remote.middleware.TokenInterceptor
import com.test_korena.data.repository.service.ServiceGenerator
import dagger.Module
import dagger.Provides
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton


@Module
class NetworkModule {
    companion object {
        const val AUTHORIZATION = "Authorization"
        private const val CONNECT_TIME_OUT_IN_SECOND = 30L
        private const val READ_TIME_OUT_IN_SECOND = 30L
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Singleton
    @Provides
    fun provideTokenInterceptor(accessTokenWrapper: AccessTokenWrapper): TokenInterceptor {
        return TokenInterceptor(accessTokenWrapper)
    }

    @Singleton
    @Provides
    fun provideRetrofitClient(
        gson: Gson,
        tokenInterceptor: TokenInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): RemoteApi {
        return ServiceGenerator.generate(
            BuildConfig.BASE_URL, RemoteApi::class.java, gson, tokenInterceptor,
            httpLoggingInterceptor
        )
    }
}