package com.test_korena.data.repository.local

import com.test_korena.data.model.DetailPage
import com.test_korena.data.model.local.CategoryLocal
import com.test_korena.data.model.local.MartInfoLocal
import com.test_korena.data.repository.local.api.DatabaseApi
import io.reactivex.Single
import javax.inject.Inject

class MartInfoLocalDataSource @Inject constructor(private val databaseApi: DatabaseApi) :
    BaseLocalDataSource() {

    fun saveCategories(categoryLocal: CategoryLocal): Single<Any> {
        return Single.create { emmit ->
            emmit.onSuccess(databaseApi.martInfoDAO().updateCategories(categoryLocal))
        }
    }

    fun getCategoriesLocal(): Single<CategoryLocal> {
        return Single.create { emmit ->
            emmit.onSuccess(databaseApi.martInfoDAO().getCategoriesLocal())
        }
    }

    fun saveMartInfo(martInfoLocal: MartInfoLocal): Single<Any> {
        return Single.create { emmit ->
            emmit.onSuccess(databaseApi.martInfoDAO().updateMartInfo(martInfoLocal))
        }
    }

    fun getMartInLocal(categoryId: String, pageNum: Int, userId: Int): Single<MartInfoLocal> {
        return Single.create { emmit ->
            emmit.onSuccess(databaseApi.martInfoDAO().getMartInfoLocal(categoryId, pageNum, userId))
        }
    }

    fun saveDetailPage(detailPage: DetailPage): Single<Any> {
        return Single.create { emmit ->
            emmit.onSuccess(databaseApi.martInfoDAO().updateDetailPage(detailPage))
        }
    }

    fun getDetailPageLocal(boardId: Int, userId: Int): Single<DetailPage> {
        return Single.create { emmit ->
            emmit.onSuccess(databaseApi.martInfoDAO().getDetailPageLocal(boardId, userId))
        }
    }
}