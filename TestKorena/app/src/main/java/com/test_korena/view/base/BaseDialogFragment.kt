package com.test_korena.view.base

import android.os.Bundle
import android.view.View
import com.test_korena.utils.navigation.Navigator
import dagger.android.support.DaggerDialogFragment
import javax.inject.Inject

abstract class BaseDialogFragment : DaggerDialogFragment() {
    @Inject
    lateinit var navigator: Navigator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        handleEvent()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observe()
    }

    open fun initView() {}

    open fun handleEvent() {}

    open fun observe() {}

    private fun showNoInternetConnectionSnackbar() {
        this.view?.let {
            navigator.showNoInternetConnectionSnackbarInDialog(it)

        }
    }

    private fun showErrorSnackbar(errorMsg: String) {
        this.view?.let {
            navigator.showErrorSnackbarInDialog(it, errorMsg)
        }
    }

    fun showMessageNotImplementYet() {
        navigator.showErrorSnackbar(requireActivity(), "This function not available")
    }

    open fun onBackPressed() {}
}