package com.test_korena.data.repository.remote.middleware

import com.test_korena.data.repository.local.api.AccessTokenWrapper
import okhttp3.*
import javax.inject.Inject

/**
 * currently, server can not response 401 code in case unauth, so we can not using this approach
 */
class TokenInterceptor @Inject constructor(private val accessTokenWrapper: AccessTokenWrapper) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder().method(original.method(), original.body())
        return chain.proceed(builder.build())
    }
}
