package com.test_korena.view.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.WebSettings
import androidx.lifecycle.Observer
import com.test_korena.R
import com.test_korena.data.model.DetailPage
import com.test_korena.utils.helper.NetworkUtil
import com.test_korena.utils.imageloader.loadImage
import com.test_korena.utils.viewextension.gone
import com.test_korena.utils.viewextension.safeClick
import com.test_korena.utils.viewextension.visible
import com.test_korena.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_detail_page.*
import kotlinx.android.synthetic.main.layout_bottom_mart_info.*
import javax.inject.Inject

class DetailPageActivity : BaseActivity() {

    companion object {
        const val KEY_ITEM_ID = "ITEM_ID"
    }

    @Inject
    lateinit var viewModel: DetailPageViewViewModel

    private var idItem: Int = -1
    private var userId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val w: Window = window
        w.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        setContentView(R.layout.activity_detail_page)
        userId = sharedPrefApi.getUserId()
        initView()
        handleEvent()
        observerData()
    }

    private fun observerData() {
        viewModel.isShowLoading.observe(this, Observer {
            if (it) {
                prbLoading.visible()
            } else {
                prbLoading.gone()
            }
        })

        viewModel.dataDetailSuccess.observe(this, Observer {
            it?.let { response ->
                updateData(response)
                response.userId = userId
                viewModel.saveDetailToLocal(response)
            }
        })

        viewModel.dataDetailError.observe(this, Observer {
            Log.e("xxx", it.message.toString())
        })

        viewModel.dataSaveDetailToLocalSuccess.observe(this, Observer {
            Log.e("xxx", "dataSaveDetailToLocalSuccess")
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun updateData(detailPage: DetailPage) {
        loadImage(htabHeader, detailPage.imgPath)
        tvCategoryName.text = detailPage.category
        tvTitle.text = detailPage.title
        tvInDate.text = detailPage.inDate

        tvLike.text = detailPage.likeCnt.toString()
        tvComment.text = detailPage.replyCnt.toString()

        wvDetail.settings.setAppCachePath(applicationContext.cacheDir.absolutePath)
        wvDetail.settings.allowContentAccess = true
        wvDetail.settings.setAppCacheEnabled(true)
        wvDetail.settings.javaScriptEnabled = true
        if (!NetworkUtil.isNetworkConnected(this)) {
            wvDetail.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        }
        wvDetail.loadDataWithBaseURL(null, detailPage.contents, "text/html", "UTF-8", null)
    }

    private fun initView() {
        if (intent.hasExtra(KEY_ITEM_ID)) {
            idItem = intent.getIntExtra(KEY_ITEM_ID, -1)
        }
        if (!NetworkUtil.isNetworkConnected(this)) {
            viewModel.getDetailLocal(idItem, userId)
        } else {
            viewModel.getDetail(sharedPrefApi.getUserId(), idItem)
        }
    }

    private fun handleEvent() {
        ivBack.safeClick(View.OnClickListener {
            finish()
        })
    }
}