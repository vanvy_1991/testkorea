package com.test_korena.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "testEntity")
class TestModel(
    @PrimaryKey
    val primaryKey: Int
)