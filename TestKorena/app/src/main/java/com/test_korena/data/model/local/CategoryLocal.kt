package com.test_korena.data.model.local

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.test_korena.data.model.Category
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "CategoryLocal")
@Parcelize
data class CategoryLocal(
    @TypeConverters(ListTypeConverters::class)
    var categories: List<Category>
) : Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = false)
    var primaryKey: Int = 0
}