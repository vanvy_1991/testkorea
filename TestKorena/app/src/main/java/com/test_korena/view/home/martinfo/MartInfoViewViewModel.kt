package com.test_korena.view.home.martinfo

import androidx.lifecycle.MutableLiveData
import com.test_korena.data.model.local.CategoryLocal
import com.test_korena.data.model.local.MartInfoLocal
import com.test_korena.data.repository.MartInfoRepository
import com.test_korena.data.repository.remote.response.CategoryResponse
import com.test_korena.data.repository.remote.response.MartInfoResponse
import com.test_korena.view.base.BaseViewModel
import dagger.Module
import javax.inject.Inject

@Module
class MartInfoViewViewModel @Inject constructor(var martInfoRepository: MartInfoRepository) :
    BaseViewModel() {
    var isShowLoading = MutableLiveData<Boolean>()
    var dataCategorySuccess = MutableLiveData<CategoryResponse>()
    var dataCategoryError = MutableLiveData<Throwable>()

    var dataMartInfoSuccess = MutableLiveData<MartInfoResponse>()
    var dataMartInfoError = MutableLiveData<Throwable>()

    var dataSaveMartInfoToLocalSuccess = MutableLiveData<Any>()
    var dataSaveMartInfoToLocalError = MutableLiveData<Throwable>()

    var dataSaveCategoryToLocalSuccess = MutableLiveData<Any>()
    var dataSaveCategoryToLocalError = MutableLiveData<Throwable>()

    var dataMartInfoFromLocalSuccess = MutableLiveData<MartInfoLocal>()
    var dataMartInfoFromLocalError = MutableLiveData<Throwable>()

    var dataCategoriesFromLocalSuccess = MutableLiveData<CategoryLocal>()
    var dataCategoriesFromLocalError = MutableLiveData<Throwable>()

    fun getCategories() {
        compositeDisposable.add(martInfoRepository.getCategories()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
//            .doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { response ->
                    dataCategorySuccess.value = response
                }
            }, { dataCategoryError.value = it })
        )
    }

    fun getMartInfo(idUser: Int, page: Int, categoryId: String) {
        compositeDisposable.add(martInfoRepository.getMartInfo(idUser, page, categoryId)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { response ->
                    dataMartInfoSuccess.value = response
                }
            }, { dataMartInfoError.value = it })
        )
    }

    fun saveCategoriesToLocal(categoryLocal: CategoryLocal) {
        compositeDisposable.add(martInfoRepository.saveCategoriesToLocal(categoryLocal)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { response ->
                    dataSaveCategoryToLocalSuccess.value = response
                }
            }, { dataSaveCategoryToLocalError.value = it })
        )
    }

    fun saveMartInfoToLocal(martInfoLocal: MartInfoLocal) {
        compositeDisposable.add(martInfoRepository.saveMartInfoToLocal(martInfoLocal)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { response ->
                    dataSaveMartInfoToLocalSuccess.value = response
                }
            }, { dataSaveMartInfoToLocalError.value = it })
        )
    }

    fun getCategoriesLocal() {
        compositeDisposable.add(martInfoRepository.getCategoriesLocal()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }.doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { response ->
                    dataCategoriesFromLocalSuccess.value = response
                }
            }, { dataCategoriesFromLocalError.value = it })
        )
    }

    fun getMartInfoLocal(categoryId: String, pageNum: Int, userId: Int) {
        compositeDisposable.add(martInfoRepository.getMartInfoLocal(categoryId, pageNum, userId)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                it?.let { response ->
                    dataMartInfoFromLocalSuccess.value = response
                }
            }, { dataMartInfoFromLocalError.value = it })
        )
    }
}