package com.test_korena.data.repository.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.test_korena.data.model.MartInfo
import kotlinx.android.parcel.Parcelize

/**
 * "list_cnt": 10,
"page_num": "1",
"total_page": 1,
 */
@Parcelize
data class MartInfoResponse constructor(

    @SerializedName("list_cnt")
    @Expose
    var listCnt: Int,
    @SerializedName("page_num")
    @Expose
    var pageNum: Int,
    @SerializedName("total_page")
    @Expose
    var totalPage: Int,

    @SerializedName("data_array")
    @Expose
    var martInfo: MutableList<MartInfo>
) : BaseResponse() {
}