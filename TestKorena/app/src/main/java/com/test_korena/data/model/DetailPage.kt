package com.test_korena.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * {
"code": "1000",
"code_msg": "정상적으로 처리되었습니다.",
"list_cnt": 0,
"product_array": [],
"board_idx": "17",
"corp_idx": "0",
"contents_yn": "Y",
"title": "5월 제철식재료 딸기",
"board_img": "http://dev-admin.martjangbogo.com/media/commonfile/202005/08/f891bab61fabf768e98b6d1f95ab7d41.png",
"view_cnt": "651",
"contents": "<!DOCTYPE html>\r\n\t\t\t\t\t<html lang=\"ko\" style=\"width:100%\">\r\n\t\t\t\t\t<head>\r\n\t\t\t\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\r\n\t\t\t\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\r\n\t\t\t\t\t<link rel=\"stylesheet\" href=\"/css/style.css\">\r\n\t\t\t\t\t<link href=\"http://www.jqueryscript.net/css/jquerysctipttop.css\" rel=\"stylesheet\" type=\"text/css\">\r\n\t\t\t\t\t<style>\r\n\t\t\t\t\t\t.video_wrap {position: relative; height:0; padding-bottom: 56.25%; margin: 0px 0px;}\r\n\t\t\t\t\t\t.video_wrap > iframe { position: absolute; width:100%; height:100%; }\r\n\t\t\t\t\t\t.instagram_wrap {position: relative; height:0; padding-bottom: 128%; }\r\n\t\t\t\t\t\t.instagram_wrap > iframe { position: absolute; width:100%; height:100%; }\r\n\t\t\t\t\t</style>\r\n\t\t\t\t\t</head>\r\n\t\t\t\t\t<body style=\"width:100%\">\r\n\t\t\t\t\t\t<div id=\"container\" style=\"max-width:100%\"><p><img src=\"http://dev-admin.martjangbogo.com/media/commonfile/202005/08/35822b3f90d6a2cea6e86de98ca02f1a.jpg\" style=\"max-width: 100%; width: 100%;\"><br></p></div>\r\n\t\t\t\t\t</body></html>",
"ins_date": "2020.05.05",
"reply_cnt": "0",
"like_cnt": "0",
"my_like_yn": "N",
"my_scrap_yn": "N",
"corp_name": null,
"category": "오늘뭐먹지"
}
 */
@Entity(tableName = "DetailPage")
@Parcelize
data class DetailPage constructor(
    @SerializedName("board_idx")
    @Expose
    var boardId: Int,
    @SerializedName("corp_idx")
    @Expose
    var corpId: Int,
    @SerializedName("contents_yn")
    @Expose
    var contentYn: String,
    @SerializedName("title")
    @Expose
    var title: String,
    @SerializedName("board_img")
    @Expose
    var imgPath: String,
    @SerializedName("view_cnt")
    @Expose
    var viewCnt: Int,
    @SerializedName("contents")
    @Expose
    var contents: String,
    @SerializedName("ins_date")
    @Expose
    var inDate: String,
    @SerializedName("reply_cnt")
    @Expose
    var replyCnt: Int,
    @SerializedName("like_cnt")
    @Expose
    var likeCnt: Int,
    @SerializedName("my_like_yn")
    @Expose
    var isMyLike: String,
    @SerializedName("my_scrap_yn")
    @Expose
    var isMyScrap: String,
    @SerializedName("category")
    @Expose
    var category: String
) : Parcelable {

    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true)
    var primaryKey: Int = 0

    var userId: Int = 0
}