package com.test_korena.view.home

import com.test_korena.di.scope.FragmentScope
import com.test_korena.view.home.martinfo.MartInfoViewFragment
import com.test_korena.view.home.martinfo.MartInfoViewModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeViewModule {
    @ContributesAndroidInjector(modules = [MartInfoViewModule::class])
    @FragmentScope
    abstract fun bindMartInfoFragment(): MartInfoViewFragment
}