package com.test_korena.view.login

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.ContextThemeWrapper
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import com.test_korena.R
import com.test_korena.utils.helper.CommonUtil.Companion.KEY_SAVE_LOGIN
import com.test_korena.utils.helper.CommonUtil.Companion.KEY_USER
import com.test_korena.utils.helper.NetworkUtil
import com.test_korena.utils.helper.hideSoftKeyboard
import com.test_korena.utils.helper.isValidEmail
import com.test_korena.utils.viewextension.gone
import com.test_korena.utils.viewextension.safeClick
import com.test_korena.utils.viewextension.visible
import com.test_korena.view.base.BaseActivity
import com.test_korena.view.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: LoginViewViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setHeader(true, "", false)
        initView()
        handleEvent()
        observerData()
    }

    private fun initView() {
        val findId = getString(R.string.find_id)
        val forgetPassword = getString(R.string.forget_password)
        val findIdSpan = SpannableString(findId)
        val forgetPasswordSpan = SpannableString(forgetPassword)

        findIdSpan.setSpan(UnderlineSpan(), 0, findId.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        forgetPasswordSpan.setSpan(
            UnderlineSpan(),
            0,
            forgetPassword.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tvFindId.text = findIdSpan
        tvForgetPassword.text = forgetPasswordSpan
    }

    private fun handleEvent() {
        tvLogin.safeClick(View.OnClickListener {
            checkLogin()
        })

        edtPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                checkLogin()
                true
            } else {
                false
            }
        }
    }

    private fun observerData() {
        viewModel.isShowLoading.observe(this, Observer {
            if (it) {
                prgLoading.visible()
            } else {
                prgLoading.gone()
            }
        })

        viewModel.dataLoginSuccess.observe(this, Observer {
            it?.let { user ->
                if (user.code == 1000) {
                    if (cbSaveLogin.isChecked) {
                        sharedPrefApi.put(KEY_USER, user)
                    }
                    navigator.startActivityAtRoot(this, HomeActivity::class.java)
                } else {
                    val builder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.Widget_AppCompat_ButtonBar_AlertDialog))
                    with(builder)
                    {
                        setTitle("Error")
                        setMessage(user.codeMsg)
                        setPositiveButton("OK", null)
                        show()
                    }
                }
            }
        })

        viewModel.dataLoginError.observe(this, Observer {

            navigator.showErrorSnackbar(this, "Error: " + it.message)
        })
    }

    private fun checkLogin() {
        hideSoftKeyboard(this)
        val email = edtEmail.text.toString()
        val password = edtPassword.text.toString()
        if (email.isBlank() || password.isBlank()) {
            navigator.showErrorSnackbar(this, getString(R.string.required_input_email_password))
            return
        }
        if (!isValidEmail(edtEmail.text.toString())) {
            navigator.showErrorSnackbar(this, getString(R.string.email_invalidate))
            return
        }
        if (NetworkUtil.isNetworkConnected(this)) {
            viewModel.login(edtEmail.text.toString(), edtPassword.text.toString())
        } else {
            navigator.showErrorSnackbar(this, getString(R.string.no_internet_connection_available))
            return
        }
    }
}