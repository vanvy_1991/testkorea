package com.test_korena.data.repository.remote.api

import com.test_korena.data.model.DetailPage
import com.test_korena.data.model.User
import com.test_korena.data.repository.remote.response.CategoryResponse
import com.test_korena.data.repository.remote.response.MartInfoResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteApi {
    @GET("login_v_1_0_0/member_login")
    fun login(
        @Query("member_id") memberId: String,
        @Query("member_pw") password: String,
        @Query("gcm_key") gcmKey: String,
        @Query("device_os") deviceOS: String
    ): Single<User>

    @GET("board_v_1_0_0/channel_category_list")
    fun getCategories(): Single<CategoryResponse>

    @GET("board_v_1_0_0/channel_list")
    fun getMartInfo(
        @Query("member_idx") idUser: Int,
        @Query("page_num") page: Int,
        @Query("category_idx") categoryId: String
    ): Single<MartInfoResponse>

    @GET("board_v_1_0_0/channel_detail")
    fun getMartDetail(
        @Query("member_idx") idUser: Int,
        @Query("board_idx") idItem: Int
    ): Single<DetailPage>
}