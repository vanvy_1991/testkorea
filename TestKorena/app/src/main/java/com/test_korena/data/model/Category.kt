package com.test_korena.data.model

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * {
"category_idx": "1",
"category_name": "오늘뭐먹지",
"ins_date": "2020-04-23 10:59:26"
}
 */

@Parcelize
data class Category constructor(
    @SerializedName("category_idx")
    @Expose
    var id: String,
    @SerializedName("category_name")
    @Expose
    var name: String,
    @SerializedName("ins_date")
    @Expose
    var inDate: String
) : Parcelable {

}