package com.test_korena.view.detail

import androidx.lifecycle.MutableLiveData
import com.test_korena.data.model.DetailPage
import com.test_korena.data.repository.MartInfoRepository
import com.test_korena.view.base.BaseViewModel
import javax.inject.Inject

class DetailPageViewViewModel @Inject constructor(private var martInfoRepository: MartInfoRepository) :
    BaseViewModel() {

    var isShowLoading = MutableLiveData<Boolean>()
    var dataDetailSuccess = MutableLiveData<DetailPage>()
    var dataDetailError = MutableLiveData<Throwable>()

    var dataSaveDetailToLocalSuccess = MutableLiveData<Any>()
    var dataSaveDetailToLocalError = MutableLiveData<Throwable>()

    fun getDetail(idUser: Int, idItem: Int) {
        compositeDisposable.add(martInfoRepository.getMartDetail(idUser, idItem)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                dataDetailSuccess.value = it
            }, {
                dataDetailError.value = it
            })
        )
    }

    fun saveDetailToLocal(detailPage: DetailPage) {
        compositeDisposable.add(martInfoRepository.saveDetailToLocal(detailPage)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                dataSaveDetailToLocalSuccess.value = it
            }, {
                dataSaveDetailToLocalError.value = it
            })
        )

    }

    fun getDetailLocal(boardId: Int, userId: Int) {
        compositeDisposable.add(martInfoRepository.getDetailPageLocal(boardId, userId)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe {
                isShowLoading.value = true
            }
            .doFinally { isShowLoading.value = false }
            .subscribe({
                dataDetailSuccess.value = it
            }, {
                dataDetailError.value = it
            })
        )

    }
}