package com.test_korena.view.base

import android.os.Bundle
import android.view.View
import com.test_korena.utils.navigation.Navigator
import com.test_korena.data.repository.local.api.SharedPrefApi
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment() {
    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var sharedPrefApi: SharedPrefApi

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        handleEvent()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeLiveData()
    }

    open fun initView() {}

    open fun handleEvent() {}

    open fun observeLiveData() {}

    open fun onNetworkAvailable() {}

    private fun showNoInternetConnectionSnackbar() {
        activity?.let {
            navigator.showNoInternetConnectionSnackbar(it)
        }
    }

    private fun showErrorSnackbar(errorMsg: String) {
        activity?.let {
            navigator.showErrorSnackbar(it, errorMsg)
        }
    }

    fun showSuccessSnackbar(successMsg: String) {
        activity?.let {
            navigator.showErrorSnackbar(it, successMsg)
        }
    }

    fun showMessageNotImplementYet() {
        navigator.showErrorSnackbar(requireActivity(), "This function not available")
    }

    open fun onBackPressed() {}

    fun setHeader(
        isShowIconLeft: Boolean,
        title: String,
        isShowIconRight: Boolean
    ){
        (requireActivity() as BaseActivity).setHeader(isShowIconLeft,title,isShowIconRight)
    }
}