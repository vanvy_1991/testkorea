package com.test_korena.data.repository

import com.test_korena.data.model.DetailPage
import com.test_korena.data.model.local.CategoryLocal
import com.test_korena.data.model.local.MartInfoLocal
import com.test_korena.data.repository.local.MartInfoLocalDataSource
import com.test_korena.data.repository.remote.datasource.MartInfoDataSource
import com.test_korena.data.repository.remote.response.CategoryResponse
import com.test_korena.data.repository.remote.response.MartInfoResponse
import io.reactivex.Single
import javax.inject.Inject

class MartInfoRepository @Inject constructor(
    private val martInfoDataSource: MartInfoDataSource,
    private val martInfoLocalDataSource: MartInfoLocalDataSource
) :
    BaseRepository() {

    fun getCategories(): Single<CategoryResponse> {
        return martInfoDataSource.getCategories()
    }

    fun getMartInfo(idUser: Int, page: Int, categoryId: String): Single<MartInfoResponse> {
        return martInfoDataSource.getMartInfo(idUser, page, categoryId)
    }

    fun getMartDetail(idUser: Int, idItem: Int): Single<DetailPage> {
        return martInfoDataSource.getMartDetail(idUser, idItem)
    }

    fun saveCategoriesToLocal(categoryLocal: CategoryLocal): Single<Any> {
        return martInfoLocalDataSource.saveCategories(categoryLocal)
    }

    fun saveMartInfoToLocal(martInfoLocal: MartInfoLocal): Single<Any> {
        return martInfoLocalDataSource.saveMartInfo(martInfoLocal)
    }

    fun saveDetailToLocal(detailPage: DetailPage): Single<Any> {
        return martInfoLocalDataSource.saveDetailPage(detailPage)
    }

    fun getCategoriesLocal(): Single<CategoryLocal> {
        return martInfoLocalDataSource.getCategoriesLocal()
    }

    fun getMartInfoLocal(categoryId: String, pageNum: Int, userId: Int): Single<MartInfoLocal> {
        return martInfoLocalDataSource.getMartInLocal(categoryId, pageNum, userId)
    }

    fun getDetailPageLocal(boardId: Int, userId: Int): Single<DetailPage> {
        return martInfoLocalDataSource.getDetailPageLocal(boardId, userId)
    }
}