package com.test_korena.view.base

import androidx.lifecycle.ViewModel
import com.test_korena.utils.scheduler.AppSchedulerProvider
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    val schedulerProvider = AppSchedulerProvider()

    val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}