package com.test_korena.data.repository

import com.test_korena.data.model.User
import com.test_korena.data.repository.remote.datasource.LoginRemoteDataSource
import io.reactivex.Single
import javax.inject.Inject

class LoginRepository @Inject constructor(private val loginRemoteDataSource: LoginRemoteDataSource) :
    BaseRepository() {

    fun login(memberId: String, memberPassword: String): Single<User> {
        return loginRemoteDataSource.login(memberId, memberPassword)
    }
}