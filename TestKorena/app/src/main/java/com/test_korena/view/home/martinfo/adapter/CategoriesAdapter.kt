package com.test_korena.view.home.martinfo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.test_korena.R
import com.test_korena.data.model.Category
import com.test_korena.utils.viewextension.safeClick
import kotlinx.android.synthetic.main.layout_item_category.view.*

class CategoriesAdapter constructor(
    var positionSelected: Int, var categories: MutableList<Category>
    , private val listener: OnItemCategoryClickListener
) :
    RecyclerView.Adapter<CategoriesAdapter.ItemCategoryViewHolder>() {


    fun updatePositionSelected(position: Int) {
        positionSelected = position
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCategoryViewHolder {
        return ItemCategoryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_item_category, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: ItemCategoryViewHolder, position: Int) {
        with(holder) {
            onBind(positionSelected, categories[position], position, listener)
        }

    }

    class ItemCategoryViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(
            positionSelected: Int,
            category: Category,
            position: Int,
            listener: OnItemCategoryClickListener
        ) {
            with(itemView) {

                if (positionSelected == position) {
                    tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.tea_lish))
                } else {
                    tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.black1))
                }

                tvCategoryName.text = category.name
            }

            itemView.safeClick(View.OnClickListener {
                listener.onItemCategoryClick(position)
            })
        }
    }

    interface OnItemCategoryClickListener {
        fun onItemCategoryClick(position: Int)

    }
}