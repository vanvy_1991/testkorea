package com.test_korena.view.home.martinfo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.test_korena.R
import com.test_korena.data.model.Category
import com.test_korena.data.model.MartInfo
import com.test_korena.data.model.local.CategoryLocal
import com.test_korena.data.model.local.MartInfoLocal
import com.test_korena.utils.helper.NetworkUtil
import com.test_korena.utils.viewextension.gone
import com.test_korena.utils.viewextension.visible
import com.test_korena.view.base.BaseFragment
import com.test_korena.view.detail.DetailPageActivity
import com.test_korena.view.detail.DetailPageActivity.Companion.KEY_ITEM_ID
import com.test_korena.view.home.martinfo.adapter.CategoriesAdapter
import com.test_korena.view.home.martinfo.adapter.MartInfoAdapter
import kotlinx.android.synthetic.main.fragment_mart_info.*
import kotlinx.android.synthetic.main.layout_loadmore.*
import kotlinx.android.synthetic.main.layout_no_data.*
import javax.inject.Inject

class MartInfoViewFragment : BaseFragment(), CategoriesAdapter.OnItemCategoryClickListener,
    MartInfoAdapter.OnItemMartInfoClickListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModel: MartInfoViewViewModel

    private var martInfoAdapter: MartInfoAdapter? = null
    private var categoryAdapter: CategoriesAdapter? = null
    private var categories = mutableListOf<Category>()
    private var listMartInfo = mutableListOf<MartInfo>()
    private var positionSelected = 0
    private var pageNum = 1
    private var userId: Int = 0
    private var isPullToRefresh: Boolean = false
    private var isLoadMore: Boolean = false
    private var isLoading: Boolean = false

    private val visibleThreshold = 5
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var totalPage: Int = 0

    companion object {
        fun newInstance(): MartInfoViewFragment {
            return MartInfoViewFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mart_info, container, false)
    }

    override fun initView() {
        super.initView()
        userId = sharedPrefApi.getUserId()

        setHeader(false, requireActivity().getString(R.string.changbo_channel), true)
        initAdapter()
        callApiCategory()
    }

    override fun observeLiveData() {
        super.observeLiveData()

        viewModel.dataSaveCategoryToLocalSuccess.observe(requireActivity(), Observer {
            Log.e("xxx", "dataSaveCategoryToLocalSuccess")
        })
        viewModel.dataSaveCategoryToLocalError.observe(requireActivity(), Observer {
            Log.e("xxx", "dataSaveCategoryToLocalError: " + it.message.toString())
        })

        viewModel.dataCategorySuccess.observe(requireActivity(), Observer {
            it?.let { response ->
                val categoryLocal = CategoryLocal(response.categories.toList())
                viewModel.saveCategoriesToLocal(categoryLocal)
                updateDateCategory(response.categories)
                callApiMartInfo()

            }
        })

        viewModel.dataCategoryError.observe(requireActivity(), Observer {
            Log.e("xxx dataCategoryError:", it?.message.toString())
        })

        viewModel.isShowLoading.observe(requireActivity(), Observer {
            if (it && !isPullToRefresh) {
                prbLoading.visible()
            } else {
                swContainer.isRefreshing = false
                prgLoadMore.gone()
                prbLoading.gone()
            }
        })
        viewModel.dataMartInfoSuccess.observe(requireActivity(), Observer {
            it?.let { response ->
                saveMartInfoToLocal(response.martInfo)
                totalPage = response.totalPage
                updateMartInfoData(response.martInfo)
            }
            checkNoData()
        })

        viewModel.dataSaveMartInfoToLocalSuccess.observe(requireActivity(), Observer {
            Log.e("xxx", "dataSaveMartInfoToLocalSuccess")
        })

        viewModel.dataSaveMartInfoToLocalError.observe(requireActivity(), Observer {
            Log.e("xxx", "dataSaveMartInfoToLocalError: " + it.message.toString())
        })

        viewModel.dataCategoriesFromLocalSuccess.observe(requireActivity(), Observer {
            it?.let { response ->
                updateDateCategory(response.categories.toMutableList())
                callApiMartInfo()
            }
        })

        viewModel.dataMartInfoFromLocalError.observe(requireActivity(), Observer {
            Log.e("xxx", "dataMartInfoFromLocalError: " + it.message.toString())
        })

        viewModel.dataMartInfoFromLocalSuccess.observe(requireActivity(), Observer {
            it?.let { response ->
                updateMartInfoData(response.martInfo.toMutableList())
            }
            checkNoData()
        })
    }

    private fun callApiMartInfo() {
        if (!NetworkUtil.isNetworkConnected(requireActivity())) {
            viewModel.getMartInfoLocal(categories[positionSelected].id, pageNum, userId)
        } else {
            viewModel.getMartInfo(userId, pageNum, categories[positionSelected].id)
        }
    }

    private fun callApiCategory() {
        if (!NetworkUtil.isNetworkConnected(requireActivity())) {
            viewModel.getCategoriesLocal()
        } else {
            viewModel.getCategories()
        }
    }

    private fun updateMartInfoData(martInfo: MutableList<MartInfo>) {
        if (!isLoadMore) {
            listMartInfo.clear()
        } else {
            isLoading = false
        }
        listMartInfo.addAll(martInfo)
        martInfoAdapter?.notifyDataSetChanged()
    }

    private fun saveMartInfoToLocal(martInfo: MutableList<MartInfo>) {
        val martInfoLocal =
            MartInfoLocal(martInfo.toList(), categories[positionSelected].id, pageNum, userId)
        viewModel.saveMartInfoToLocal(martInfoLocal)
    }

    private fun checkNoData() {
        if (listMartInfo.size == 0) {
            clNoData.visible()
        } else {
            clNoData.gone()
        }
    }

    override fun handleEvent() {
        super.handleEvent()
        swContainer.setOnRefreshListener(this)

        rcvMartInfo.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                totalItemCount = recyclerView.layoutManager?.itemCount ?: 0
                lastVisibleItem =
                    (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        isLoading = true
                        isLoadMore = true
                        prgLoadMore.visible()
                        loadMore()
                    }
                }
            }
        })
    }

    private fun loadMore() {
        pageNum += 1
        if (totalPage < pageNum) {
            return
        }
        viewModel.getMartInfo(userId, pageNum, categories[positionSelected].id)
    }

    private fun updateDateCategory(categoriesData: MutableList<Category>) {
        categories.addAll(categoriesData)
        categoryAdapter?.notifyDataSetChanged()
    }

    private fun initAdapter() {
        categoryAdapter = CategoriesAdapter(positionSelected, categories, this)
        rcvCategory.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        rcvCategory.adapter = categoryAdapter

        martInfoAdapter = MartInfoAdapter(listMartInfo, this)
        rcvMartInfo.layoutManager = LinearLayoutManager(requireActivity())
        rcvMartInfo.adapter = martInfoAdapter
    }

    override fun onItemCategoryClick(position: Int) {
        clNoData.gone()
        totalItemCount = 0
        lastVisibleItem = 0
        pageNum = 1
        isPullToRefresh = false
        isLoadMore = false
        positionSelected = position
        categoryAdapter?.updatePositionSelected(positionSelected)

        //todo get list item
        callApiCategory()
    }

    override fun onItemMartInfoClick(position: Int) {
        val bundle = Bundle()
        bundle.putInt(KEY_ITEM_ID, listMartInfo[position].id)
        navigator.startActivity(requireActivity(), DetailPageActivity::class.java, bundle)
    }

    override fun onRefresh() {
        //pull to refresh
        totalItemCount = 0
        lastVisibleItem = 0
        isLoading = false
        isLoadMore = false
        isPullToRefresh = true
        pageNum = 1
        callApiCategory()
    }
}