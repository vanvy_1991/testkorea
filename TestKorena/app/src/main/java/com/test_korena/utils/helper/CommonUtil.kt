package com.test_korena.utils.helper

import android.content.Context
import android.text.TextUtils
import android.util.Patterns
import android.util.TypedValue


class CommonUtil {
    companion object {
        const val KEY_SAVE_LOGIN = "KEY_SAVE_LOGIN"
        const val KEY_USER = "USER"
    }
}

fun isValidEmail(target: CharSequence): Boolean {
    return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
}

fun convertDpToPx(context: Context, dp: Float): Int {
    val r = context.resources

    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        r.displayMetrics
    ).toInt()
}